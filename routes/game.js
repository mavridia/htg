var express = require('express');
var router = express.Router();

const { ensureAuthenticated, isGuest } = require('../middleware/auth');

const GameController = require('../controllers/game');

const path = require('path');


router.route('/')
	.get(function (req, res, next) {

		res.sendFile(path.join(__dirname, '../views', 'index.html'))
	});

router.route('/start-game')
	.get(ensureAuthenticated, GameController.startGame);

router.route('/answer')
	.post(ensureAuthenticated, GameController.answerGame);

module.exports = router;
