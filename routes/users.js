var express = require('express');
var passport = require('passport');
var passportConf = require('../middleware/passport');
var router = express.Router();

const UsersController = require('../controllers/users');

const { validateSignup, validateLogin } = require('../validators/usersValidationHelper');
const { ensureAuthenticated, isGuest } = require('../middleware/auth');

const passportLocalAuth = passport.authenticate('local', { successRedirect:'/' , failureRedirect:'/users/login', failureFlash: true });

router.route('/login')
	.get(isGuest, function (req, res, next) {
		res.render('users/login', {
			title: 'Login',
			layout: 'login'
		});
	})
	.post(isGuest, validateLogin(), passportLocalAuth, (req, res) => {

		req.flash('success_msg','Welcome');
		res.redirect('/game');
	});


router.route('/signup')
	.get(isGuest, function (req, res, next) {
		res.render('users/signup', {
			title: 'Signup',
			layout: 'login'
		});
	})
	.post(isGuest, validateSignup(), UsersController.createUser);

router.route('/logout')
	.get(ensureAuthenticated, function (req, res, next) {
		req.logout();
		req.flash('success_msg', 'You are logged out');
		res.redirect('/');
	})

module.exports = router;