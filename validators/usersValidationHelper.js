module.exports = {
	validateSignup: () => {
		return (req, res, next) => {
			req.checkBody('firstname', 'First name is required').notEmpty();
			req.checkBody('lastname', 'Last name is required').notEmpty();
			req.checkBody('email', 'Email is required').notEmpty();
			req.checkBody('email', 'Email is not valid').isEmail();
			req.checkBody('gender', 'Gender is required').notEmpty();
			req.checkBody('password', 'Password is required').notEmpty();
			req.checkBody('confirm_password', 'Passwords do not match').equals(req.body.password);

			var errors = req.validationErrors();
			var err = [];
			for (const key in errors) {
				err[errors[key]['param']] = {
					msg: errors[key]['msg']
				};
			}

			if (errors) {
				return res.render(req.session.backURL || '/', {
					layout: 'login',
					errors: err,
					body: {
						email: req.body.email,
						firstname: req.body.firstname,
						lastname: req.body.lastname,
						male: (req.body.gender == 'male') ? 1 : 0,
						female: (req.body.gender == 'female') ? 1 : 0,
					}
				});
			} else {
				next();
			}
		}
	},

	validateLogin: () => {
		return (req, res, next) => {
			req.checkBody('email', 'Email is required').notEmpty();
			req.checkBody('email', 'Email is not valid').isEmail();
			req.checkBody('password', 'Password is required').notEmpty();

			var errors = req.validationErrors();
			var err = [];
			for (const key in errors) {
				err[errors[key]['param']] = {
					msg: errors[key]['msg']
				};
			}

			if (errors) {
				return res.render(req.session.backURL || '/', {
					layout: 'login',
					errors: err,
					body: {
						email: req.body.email
					}
				});
			} else {
				next();
			}
		}
	},
}