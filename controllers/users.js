const bcrypt = require('bcrypt');
const createError = require('http-errors');
const mongoose = require('mongoose');

const User = require('../models/users');

exports.createUser = (req, res, next) => {
	User.findOne({ 'email': req.body.email })
		.exec()
		.then((user) => {
			if (user) {
				var errors = [];
				errors['email'] = { msg: "Email already exists." };

				res.render(req.session.backURL || '/', {
					layout: 'login',
					errors
				});
			} else {
				const user = new User({
					_id: mongoose.Types.ObjectId(),
					email: req.body.email,
					password: req.body.password,
					firstname: req.body.firstname,
					lastname: req.body.lastname,
					gender: req.body.gender,
				});

				user.save()
					.then(result => {

						req.flash('success_msg', 'User created successfully!');
						res.redirect('/users/login');
					})
					.catch(err => { throw new Error(err) });
			}
		})
		.catch(err => { throw new Error(err)} );
}
