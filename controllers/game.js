const bcrypt = require('bcrypt');
const createError = require('http-errors');
const mongoose = require('mongoose');

const Game = require('../models/game');
const Question = require('../models/question');

exports.startGame = (req, res, next) => {
	if (!req.session.game) {
		var game = new Game({
			_id: mongoose.Types.ObjectId(),
			user: res.locals.user.id,
			status: 'new',
			question_index: 0,
			score: 0
		});

		req.session.game = game;

		console.log(game);

		game.save()
			.then(result => {
				Question.random((err, question) => {
					if (err) {
						throw new Error(err)
					}

					res.send({
						result: true,
						game: {
							id: game._id,
							question_index: game.question_index
						},
						question: {
							id: question.id,
							text: question.text,
							answers: question.answers.map(answer => {
								return {
									id: answer.id,
									text: answer.text,
								}
							}),
						}
					})
				});
			})
			.catch(err => { throw new Error(err) });
	} else {
		Question.random((err, question) => {
			if (err) {
				throw new Error(err)
			}

			res.send({
				result: true,
				game: {
					id: req.session.game._id,
					question_index: req.session.game.question_index
				},
				question: {
					id: question.id,
					text: question.text,
					answers: question.answers.map(answer => {
						return {
							id: answer.id,
							text: answer.text,
						}
					}),
				}
			})
		});
	}
}

exports.answerGame = (req, res, next) => {

	if (res.locals.user) {
		Question.findOne({ id: req.body.question_id }, (err, question) => {
			if (err) {
				throw new Error(err);
			}

			if (!question) {
				console.log(question);
				console.log(`There is not question with id: ${req.body.question_id}`)

				return res.send({
					result: false,
					message: 'There is no such question'
				})
			} else {
				var correct_answer = question.answers.find(answer => {
					return answer.is_correct === true
				})

				if (correct_answer.id == req.body.answer_id) {

					Game.findOne({ _id: req.body.game_id }, (err, game) => {
						if (err) {
							throw new Error(err);
						}

						game.question_index = game.question_index + 1;
						game.score = game.score + 1;
						game.save()
							.then(result => {
								Question.random((err, question) => {
									if (err) {
										throw new Error(err)
									}

									return res.send({
										result: true,
										game: {
											id: game._id,
											question_index: game.question_index
										},
										question: {
											id: question.id,
											text: question.text,
											answers: question.answers.map(answer => {
												return {
													id: answer.id,
													text: answer.text,
												}
											}),
										}
									})
								});
							})
							.catch(err => { throw new Error(err) });
					})
				} else {
					return res.send({
						result: false,
						message: 'Wrong answer. Please try again!'
					})
				}
			}

		})
	} else {
		return res.send({
			result: false,
			message: 'No session'
		})
	}
}

