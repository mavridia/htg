const bcrypt = require('bcrypt');
const mongoose = require ('mongoose');

//User Schema

const userSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
    firstname: {
		type: String,
		required: true
	},
    lastname: {
		type: String,
		required: true
	},
	email: {
		type: String,
		trim: true,
		match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
	},
	password: {
		type: String,
	},
	gender: {
		type: String,
		required: true
	}
});

userSchema.pre('save', function (next) {
	bcrypt.hash(this.password, 10, (err, hash) => {
		if (err) {
			next(err);
		} else {
			this.password = hash;
			next();
		}
	});
});

userSchema.methods.verifyPassword = async function(password) {
	try {
		return await bcrypt.compare(password, this.password);
	} catch (error) {
		throw new Error(error);
	}
}

userSchema.statics.getUserById = function (id, callback) {
	this.model('User').findOne({ _id: id}, callback);
}

module.exports = mongoose.model('User', userSchema);
