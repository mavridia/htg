const mongoose = require('mongoose');

//Question Schema

const questionSchema = mongoose.Schema({
	id: Number,
	text: {
		type: String,
		required: true
	},
	answers: {
		type: Array,
	}
});

questionSchema.statics.random = function (callback) {
	this.countDocuments(function (err, count) {
		if (err) return callback(err);
		var rand = Math.floor(Math.random() * count);
		this.findOne().skip(rand).exec(callback);
	}.bind(this));
};

questionSchema.statics.getQuestionById = function (id, callback) {
	this.model('Question').findOne({ _id: id }, callback);
}

module.exports = mongoose.model('Question', questionSchema);
