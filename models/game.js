const mongoose = require('mongoose');

//Game Schema

const gameSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	status: {
		type: String,
		required: true
	},
	question_index: {
		type: Number,
		required: true
	},
	score: {
		type: Number,
		required: false
	}
});

module.exports = mongoose.model('Game', gameSchema);
