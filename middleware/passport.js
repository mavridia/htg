const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const User = require('../models/users');

/* Local Strategy */
passport.use(new LocalStrategy({ usernameField: 'email' },
    async function (email, password, done) {
		try {
			var user = await User.findOne({ "email": email })

			if (!user) {
				return done(null, false);
			}

            var result = await user.verifyPassword(password);

            if (!result) {
				return done(null, false);
			}

			return done(null, user);

		} catch(err) {
			return done(err);
		}
    }
));


passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    var user = User.getUserById(id, (err, user) => {
		done(err, user);
	})

});