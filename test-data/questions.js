[
	{
		id: 1,
		text: 'What is the population of Heraklion?',
		answers: [
			{
				id: 2,
				text: '225,574',
				is_correct: true
			},
			{
				id: 3,
				text: '200,475',
				is_correct: false,
			},
			{
				id: 4,
				text: '180,932',
				is_correct: false,
			}, {
				id: 5,
				text: '175,870',
				is_correct: false,
			}
		],
	},
	{
		id: 6,
		text: 'In how many km2 is Heraklion extended?',
		answers: [
			{
				id: 7,
				text: '254.2',
				is_correct: false,
			},
			{
				id: 8,
				text: '294.2',
				is_correct: false,
			},
			{
				id: 9,
				text: '264.2',
				is_correct: true,
			},
			{
				id: 10,
				text: '284.2',
				is_correct: false,
			}
		],
	},
	{
		id: 11,
		text: 'When was the Venetian fortress of Castello a Mare built?',
		answers: [
			{
				id: 12,
				text: '1490–1520',
				is_correct: false,
			},
			{
				id: 13,
				text: '1500–1510',
				is_correct: false,
			},
			{
				id: 14,
				text: '1530–1550',
				is_correct: false,
			},
			{
				id: 15,
				text: '1523–1540',
				is_correct: true,
			},
		],
	},
	{
		id: 16,
		text: 'In which century was built the Saint Peter of Dominicans monument?',
		answers: [
			{
				id: 17,
				text: '11',
				is_correct: false,
			},
			{
				id: 18,
				text: '13',
				is_correct: false,
			},
			{
				id: 19,
				text: '14',
				is_correct: false,
			},
			{
				id: 20,
				text: '12',
				is_correct: true,
			}
		],
	},
	{
		id: 21,
		text: 'What is the name of the heraklion airport ?',
		answers: [
			{
				id: 22,
				text: 'Nikos Kazantzakis',
				is_correct: true,
			},
			{
				id: 23,
				text: 'Captain Vassilis Constantakopoulos',
				is_correct: false,
			},
			{
				id: 24,
				text: 'Ioannis Daskalogiannis',
				is_correct: false,
			},
			{
				id: 25,
				text: 'Eleftherios Venizelos',
				is_correct: false,
			}
		],
	}
]